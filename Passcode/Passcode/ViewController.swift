//
//  ViewController.swift
//  Passcode
//
//  Created by Evan Pierzina on 1/20/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    let passCode:String = "12369874"
    var currentCode:String = ""
    var digitCount:Int = 0
    var audioPlayer:AVAudioPlayer?
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var safeImage: UIImageView!
    @IBOutlet weak var currentCodeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func keypadPressed(sender: AnyObject) {
        checkCode(sender.titleForState(.Normal)!)
    }
    
    /* Check the input */
    func checkCode(digit:String) {
        if ++digitCount <= 8 {
            currentCode = currentCode + digit
            currentCodeLabel.text = currentCode
            if digitCount == 8 {
                if currentCode == passCode {
                    audioPlayer = setupAudioPlayerWithFile("right")
                    audioPlayer?.prepareToPlay()
                    messageLabel.text = "You have the correct combination"
                    safeImage.image = UIImage(named: "open")
                    audioPlayer?.play()
                }
                else {
                    audioPlayer = setupAudioPlayerWithFile("wrong")
                    audioPlayer?.prepareToPlay()
                    messageLabel.text = "Incorrect combination"
                    safeImage.image = UIImage(named: "redx")
                    audioPlayer?.play()
                }
            }
        }
    }
    
    /* Reinitialize the interface */
    @IBAction func reset(sender: AnyObject) {
        audioPlayer = setupAudioPlayerWithFile("reset")
        audioPlayer?.prepareToPlay()
        currentCode = ""
        currentCodeLabel.text = ""
        digitCount = 0
        messageLabel.text = "Please place the correct combination"
        safeImage.image = UIImage(named: "closed")
        audioPlayer?.play()
    }
    
    /* Initialize the audioPlayer variable */
    func setupAudioPlayerWithFile(file: String) -> AVAudioPlayer? {
        if let audioURL:NSURL = NSBundle.mainBundle().URLForResource(file, withExtension: "WAV") {
            do {
                return try AVAudioPlayer(contentsOfURL: audioURL)
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}


//
//  2DMapViewController.swift
//  Yellowstone
//
//  Created by Evan Pierzina on 2/13/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation

class _2DMapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        let utterence = AVSpeechUtterance(string: "Yellowstone National Park, Wyoming")
        let synthesizer = AVSpeechSynthesizer()
        let location = CLLocation(latitude: 44.619636, longitude: -110.560398)
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            10000, 10000)
        mapView.setRegion(coordinateRegion, animated: true)
        let pin = MKPointAnnotation()
        pin.coordinate = CLLocationCoordinate2D(latitude: 44.619636, longitude: -110.560398)
        pin.title = "Yellowstone National Park"
        pin.subtitle = "Wyoming"
        mapView.addAnnotation(pin)
        mapView.selectAnnotation(pin, animated: true)
        synthesizer.speakUtterance(utterence)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ViewController.swift
//  Yellowstone
//
//  Created by Evan Pierzina on 2/13/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit
import AVFoundation

class HomeViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let green = UIColor(red:94/255.0, green:118/255.0, blue:48/255.0, alpha: 1.0).CGColor
        image1.layer.borderWidth = 1
        image1.layer.borderColor = green
        image2.layer.borderWidth = 1
        image2.layer.borderColor = green
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func soundButtonPressed(sender: UIButton) {
        let tts = AVSpeechSynthesizer()
        tts.speakUtterance(AVSpeechUtterance(string: textView.text))
    }

}


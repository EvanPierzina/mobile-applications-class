//
//  WebViewController.swift
//  Yellowstone
//
//  Created by Evan Pierzina on 2/13/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        let url = NSURL(string: "http://www.nps.gov/yell/")
        webView.loadRequest(NSURLRequest(URL: url!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func webViewDidStartLoad(webView: UIWebView!) {
        activityIndicator.hidden = false
    }
    
    func webViewDidFinishLoad(webView: UIWebView!) {
        activityIndicator.hidden = true
    }
    
    func webView(webView: UIWebView!, didFailLoadWithError error: NSError!) {
        activityIndicator.hidden = true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

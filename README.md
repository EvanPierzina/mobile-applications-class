# README #

These are projects from my mobile applications development class. They were meant to cover the very, very basics of iOS (Swift) programming.

The projects (in the order they were assigned):
================================================================================================================================================
### Passcode: ###
Displays a keypad that allows user input. Checks against a predetermined password.

### RoadTrip: ###
Displays a map and asks for users to input where they want to go. Calculates relevant trip information, such as miles and gas needed.

### MatchMaker: ###
A matchmaking game with two modes: match the state capital to the US state and match the "famous" quote to the person who said it.

### Yellowstone: ###
An informational app about Yellowstone National Park.

### Labs: ###
An informational app about the various computer labs at UNLV.
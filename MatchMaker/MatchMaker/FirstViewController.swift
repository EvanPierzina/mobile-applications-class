//
//  FirstViewController.swift
//  MatchMaker
//
//  Created by Evan Pierzina on 2/5/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit
import AVFoundation
//import GameKit

class FirstViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var data = [[],[]]
    var counter = 0
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var correctLabel: UILabel!
    @IBOutlet weak var capitalView: UIImageView!
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var matchesLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    
    var audioPlayer:AVAudioPlayer = AVAudioPlayer()
    
    var statesFlagArray = [ false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false ]
    var capitalsFlagArray = [ false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false ]
    
    let soundsArray = [ "fantastic", "excellent", "unbelievable", "awesome", "good", "nice", "great", "wow", "super", "amazing" ]
    
    let statesArray = [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming"]
    let capitalsArray = [
        "Albany",
        "Annapolis",
        "Atlanta",
        "Augusta",
        "Austin",
        "Baton Rouge",
        "Bismarck",
        "Boise",
        "Boston",
        "Carson City",
        "Charleston",
        "Cheyenne",
        "Columbia",
        "Columbus",
        "Concord",
        "Denver",
        "Des Moines",
        "Dover",
        "Frankfort",
        "Harrisburg",
        "Hartford",
        "Helena",
        "Honolulu",
        "Indianapolis",
        "Jackson",
        "Jefferson City",
        "Juneau",
        "Lansing",
        "Lincoln",
        "Little Rock",
        "Madison",
        "Montgomery",
        "Montpelier",
        "Nashville",
        "Oklahoma City",
        "Olympia",
        "Phoenix",
        "Pierre",
        "Providence",
        "Raleigh",
        "Richmond",
        "Sacramento",
        "Salem",
        "Salt Lake City",
        "Santa Fe",
        "Springfield",
        "St. Paul",
        "Tallahassee",
        "Topeka",
        "Trenton"]
    
    let stateCapitalDictionary:NSDictionary = [
        "Alabama":"Montgomery",
        "Alaska":"Juneau",
        "Arizona":"Phoenix",
        "Arkansas":"Little Rock",
        "California":"Sacramento",
        "Colorado":"Denver",
        "Connecticut":"Hartford",
        "Delaware":"Dover",
        "Florida":"Tallahassee",
        "Georgia":"Atlanta",
        "Hawaii":"Honolulu",
        "Idaho":"Boise",
        "Illinois":"Springfield",
        "Indiana":"Indianapolis",
        "Iowa":"Des Moines",
        "Kansas":"Topeka",
        "Kentucky":"Frankfort",
        "Louisiana":"Baton Rouge",
        "Maine":"Augusta",
        "Maryland":"Annapolis",
        "Massachusetts":"Boston",
        "Michigan":"Lansing",
        "Minnesota":"St. Paul",
        "Mississippi":"Jackson",
        "Missouri":"Jefferson City",
        "Montana":"Helena",
        "Nebraska":"Lincoln",
        "Nevada":"Carson City",
        "New Hampshire":"Concord",
        "New Jersey":"Trenton",
        "New Mexico":"Santa Fe",
        "New York":"Albany",
        "North Carolina":"Raleigh",
        "North Dakota":"Bismarck",
        "Ohio":"Columbus",
        "Oklahoma":"Oklahoma City",
        "Oregon":"Salem",
        "Pennsylvania":"Harrisburg",
        "Rhode Island":"Providence",
        "South Carolina":"Columbia",
        "South Dakota":"Pierre",
        "Tennessee":"Nashville",
        "Texas":"Austin",
        "Utah":"Salt Lake City",
        "Vermont":"Montpelier",
        "Virginia":"Richmond",
        "Washington":"Olympia",
        "West Virginia":"Charleston",
        "Wisconsin":"Madison",
        "Wyoming":"Cheyenne"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        data = [statesArray, capitalsArray]
        
        matchesLabel.text = "\(counter) / \(statesArray.count) matches"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return data.count
    }
    
    @IBAction func reset(sender: UIButton) {
        sender.hidden = true
        statesFlagArray = [ false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false ]
        capitalsFlagArray = [ false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false ]

        data = [statesArray, capitalsArray]
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            dispatch_async(dispatch_get_main_queue()) {
                self.picker.reloadAllComponents()
            }
        }
        counter = 0
        matchesLabel.text = "\(counter) / \(statesArray.count) matches"
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data[component].count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        instructionLabel.hidden = false
        capitalView.hidden = true
        correctLabel.hidden = true
        
        return data[component][row] as? String
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        let state:String = self.pickerView(picker, titleForRow: picker.selectedRowInComponent(0), forComponent: 0)!
        let capital:String = self.pickerView(picker, titleForRow: picker.selectedRowInComponent(1), forComponent: 1)!
        
        if (stateCapitalDictionary.valueForKey(state) as? NSString) == capital {
            correctLabel.hidden = false
            instructionLabel.hidden = true
            capitalView.image = UIImage(named: capital)
            capitalView.contentMode = UIViewContentMode.ScaleAspectFit;
            capitalView.hidden = false
            if statesFlagArray[picker.selectedRowInComponent(0)] == false {
                counter++
                matchesLabel.text = "\(counter) / \(statesArray.count) matches"
                statesFlagArray[picker.selectedRowInComponent(0)] = true
                capitalsFlagArray[picker.selectedRowInComponent(1)] = true
                let otherComponent = component == 0 ? 1 : 0
                let otherRow = picker.selectedRowInComponent(otherComponent)
                self.pickerView(picker, viewForRow: row, forComponent: component, reusingView: picker.viewForRow(row, forComponent: component))
                self.pickerView(picker, viewForRow: otherRow, forComponent: otherComponent, reusingView: picker.viewForRow(otherRow, forComponent: otherComponent))
                
                let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                
                dispatch_async(dispatch_get_global_queue(priority, 0)) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.picker.reloadAllComponents()
                    }
                }
                
                let audioURL:NSURL = NSBundle.mainBundle().URLForResource(soundsArray[Int(arc4random_uniform(10))], withExtension: "m4a")!
                
                do {
                    try audioPlayer = AVAudioPlayer(contentsOfURL: audioURL)
                    audioPlayer.prepareToPlay()
                    audioPlayer.play()
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
                
                if counter == statesArray.count {
                    //you win!
                    //play noise
                    //throw confetti
                    //display a reset button to reinitialize everything
                    resetButton.hidden = false
                }
            }
            
        }
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let label:UILabel
        //if it's not on screen
        if view == nil {
            label = UILabel()
        }
        //if it's already on screen
        else {
            label = view as! UILabel
        }
        //change the selector bars to white
        pickerView.subviews[1].backgroundColor = UIColor.whiteColor()
        pickerView.subviews[2].backgroundColor = UIColor.whiteColor()
        //check flag array to see if the row has been matched
        let flagArray = component == 0 ? statesFlagArray : capitalsFlagArray
        if flagArray[row] == true {
            //indicate that it's been matched with green text
            label.textColor = UIColor(red: 0.282, green: 0.792, blue: 0.282, alpha: 1.0)
        }
        else {
            label.textColor = UIColor.whiteColor()
        }
        label.textAlignment = NSTextAlignment.Center
        label.font = UIFont.systemFontOfSize(UIFont.systemFontSize() + 5)
        label.text = data[component][row] as? String
        
        return label
    }
    
    func pickerView(pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return picker.frame.size.width / 3
    }
}
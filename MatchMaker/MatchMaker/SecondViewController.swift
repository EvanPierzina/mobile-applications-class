//
//  SecondViewController.swift
//  MatchMaker
//
//  Created by Evan Pierzina on 2/5/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit
import GameKit
import AVFoundation

class SecondViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var data = [[],[]]
    var counter = 0
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var correctLabel: UILabel!
    @IBOutlet weak var personView: UIImageView!
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var matchesLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    
    var audioPlayer:AVAudioPlayer = AVAudioPlayer()
    
    var peopleFlagArray = [ false, false, false, false, false ]
    var quoteFlagArray = [ false, false, false, false, false ]
    
    let soundsArray = [ "fantastic", "excellent", "unbelievable", "awesome", "good", "nice", "great", "wow", "super", "amazing" ]
    
    let peopleArray = [
        "Dr. Yfantis",
        "Arnie",
        "Ned Stark",
        "Captain Falcon",
        "Elsa"]
    let quoteArray = [
        "I am a mountain!",
        "Get to da choppa!",
        "Winter is coming",
        "Falcon Punch!",
        "Let it go"]
    
    let peopleQuoteDictionary:NSDictionary = [
        "Dr. Yfantis":"I am a mountain!",
        "Arnie":"Get to da choppa!",
        "Ned Stark":"Winter is coming",
        "Captain Falcon":"Falcon Punch!",
        "Elsa":"Let it go"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //randomize the array, otherwise it's too easy!
        let lcg = GKLinearCongruentialRandomSource.init()
        let shuffledCapitalsArray:Array = (lcg.arrayByShufflingObjectsInArray(quoteArray as [AnyObject]) as? [String])!
        data = [peopleArray, shuffledCapitalsArray]
        
        matchesLabel.text = "\(counter) / \(peopleArray.count) matches"
        personView.contentMode = UIViewContentMode.ScaleAspectFit
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func reset(sender: UIButton) {
        sender.hidden = true
        peopleFlagArray = [ false, false, false, false, false ]
        quoteFlagArray = [ false, false, false, false, false ]
        let lcg = GKLinearCongruentialRandomSource.init()
        let shuffledCapitalsArray:Array = (lcg.arrayByShufflingObjectsInArray(quoteArray as [AnyObject]) as? [String])!
        data = [peopleArray, shuffledCapitalsArray]
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            dispatch_async(dispatch_get_main_queue()) {
                self.picker.reloadAllComponents()
            }
        }
        counter = 0
        matchesLabel.text = "\(counter) / \(peopleArray.count) matches"
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return data.count
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data[component].count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        instructionLabel.hidden = false
        personView.hidden = true
        correctLabel.hidden = true
        
        return data[component][row] as? String
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        let person:String = self.pickerView(picker, titleForRow: picker.selectedRowInComponent(0), forComponent: 0)!
        let quote:String = self.pickerView(picker, titleForRow: picker.selectedRowInComponent(1), forComponent: 1)!
        
        if (peopleQuoteDictionary.valueForKey(person) as? NSString) == quote {
            correctLabel.hidden = false
            instructionLabel.hidden = true
            personView.image = UIImage(named: person)
            personView.hidden = false
            if peopleFlagArray[picker.selectedRowInComponent(0)] == false {
                counter++
                matchesLabel.text = "\(counter) / \(peopleArray.count) matches"
                peopleFlagArray[picker.selectedRowInComponent(0)] = true
                quoteFlagArray[picker.selectedRowInComponent(1)] = true
                let otherComponent = component == 0 ? 1 : 0
                let otherRow = picker.selectedRowInComponent(otherComponent)
                self.pickerView(picker, viewForRow: row, forComponent: component, reusingView: picker.viewForRow(row, forComponent: component))
                self.pickerView(picker, viewForRow: otherRow, forComponent: otherComponent, reusingView: picker.viewForRow(otherRow, forComponent: otherComponent))
                
                let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                
                dispatch_async(dispatch_get_global_queue(priority, 0)) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.picker.reloadAllComponents()
                    }
                }
                
                let audioURL:NSURL = NSBundle.mainBundle().URLForResource(soundsArray[Int(arc4random_uniform(10))], withExtension: "m4a")!
                
                do {
                    try audioPlayer = AVAudioPlayer(contentsOfURL: audioURL)
                    audioPlayer.prepareToPlay()
                    audioPlayer.play()
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
                
                if counter == peopleArray.count {
                    //you win!
                    //play noise
                    //throw confetti
                    //display a reset button to reinitialize everything
                    resetButton.hidden = false
                }
            }
            
        }
    }

    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let label:UILabel
        //if it's not on screen
        if view == nil {
            label = UILabel()
        }
        //if it's already on screen
        else {
            label = view as! UILabel
        }
        //change the selector bars to white
        pickerView.subviews[1].backgroundColor = UIColor.whiteColor()
        pickerView.subviews[2].backgroundColor = UIColor.whiteColor()
        //check flag array to see if the row has been matched
        let flagArray = component == 0 ? peopleFlagArray : quoteFlagArray
        if flagArray[row] == true {
            //indicate that it's been matched with green text
            label.textColor = UIColor(red: 0.282, green: 0.792, blue: 0.282, alpha: 1.0)
        }
        else {
            label.textColor = UIColor.whiteColor()
        }
        label.textAlignment = NSTextAlignment.Center
        label.font = UIFont.systemFontOfSize(UIFont.systemFontSize())
        label.text = data[component][row] as? String
        return label
    }
    
    func pickerView(pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return picker.frame.size.width / 3
    }
}
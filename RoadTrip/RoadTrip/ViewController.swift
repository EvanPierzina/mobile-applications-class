//
//  ViewController.swift
//  RoadTrip
//
//  Created by Evan Pierzina on 1/31/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var distanceTextField: UITextField!
    @IBOutlet weak var distanceAddButton: UIButton!
    @IBOutlet weak var distanceSetButton: UIButton!
    
    @IBOutlet weak var travelledTextField: UITextField!
    @IBOutlet weak var travelledAddButton: UIButton!
    @IBOutlet weak var travelledSetButton: UIButton!
    
    @IBOutlet weak var gasTextField: UITextField!
    @IBOutlet weak var gasAddButton: UIButton!
    @IBOutlet weak var gasSetButton: UIButton!
    
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var timeAddButton: UIButton!
    @IBOutlet weak var timeSetButton: UIButton!
    
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var priceAddButton: UIButton!
    @IBOutlet weak var priceSetButton: UIButton!
    
    @IBOutlet weak var averageSpeedLabel: UILabel!
    @IBOutlet weak var remainingDistanceLabel: UILabel!
    @IBOutlet weak var remainingTimeLeftLabel: UILabel!
    @IBOutlet weak var remainingGasCostLabel: UILabel!
    @IBOutlet weak var milesPerGallonLabel: UILabel!
    @IBOutlet weak var gasMoneyPerMileLabel: UILabel!
    
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var initialDistance: Float = 0
    var distanceTravelled: Float = 0
    var gasUsed: Float = 0
    var travelTime: Float = 0
    var pricePaidForGas: Float = 0
    let currencyFormatter: NSNumberFormatter = NSNumberFormatter()
    let decimalFormatter: NSNumberFormatter = NSNumberFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set up observers for keyboard events
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "adjustViewForKeyboard:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "adjustViewForKeyboard:", name: UIKeyboardWillHideNotification, object: nil)
        
        //set up event to dismiss keyboard on a tap outside of a text field
        //default UITapGestureRecognizer = 1 tap with 1 finger
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
        //variable initializations
        mapView.showsUserLocation = true
        currencyFormatter.numberStyle = .CurrencyStyle
        decimalFormatter.maximumFractionDigits = 1
        decimalFormatter.minimumFractionDigits = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*  UpdateStatistics
    =============================
        This method is called by any button press,
        as well as from other methods that implement
        the behavior of a button press.
        It performs the following tasks:
            +setting the member variables
            +updating text fields text and color
            +updating labels for calculations
            +hiding and disabling/enabling buttons
    */
    @IBAction func UpdateStatistics(sender: UIButton) {
        switch sender {
        case distanceAddButton:
            initialDistance = initialDistance + (distanceTextField.text! as NSString).floatValue
            distanceTextField.text = "\(initialDistance)"
            distanceTextField.textColor = UIColor.lightGrayColor()
            distanceAddButton.hidden = true
            distanceSetButton.enabled = true
            if travelledTextField.text != "" && distanceTravelled + (travelledTextField.text! as NSString).floatValue <= initialDistance {
                travelledAddButton.hidden = false
            }
            break
        case distanceSetButton:
            initialDistance = 0
            distanceTextField.text?.removeAll()
            distanceSetButton.enabled = false
            travelledAddButton.hidden = true
            break
        case travelledAddButton:
            if distanceTravelled + (travelledTextField.text! as NSString).floatValue <= initialDistance {
                distanceTravelled = distanceTravelled + (travelledTextField.text! as NSString).floatValue
                travelledTextField.text = "\(distanceTravelled)"
                travelledTextField.textColor = UIColor.lightGrayColor()
                travelledAddButton.hidden = true
                travelledSetButton.enabled = true
            }
            break
        case travelledSetButton:
            distanceTravelled = 0
            travelledTextField.text?.removeAll()
            travelledSetButton.enabled = false
            break
        case gasAddButton:
            gasUsed = gasUsed + (gasTextField.text! as NSString).floatValue
            gasTextField.text = "\(gasUsed)"
            gasTextField.textColor = UIColor.lightGrayColor()
            gasAddButton.hidden = true
            gasSetButton.enabled = true
            break
        case gasSetButton:
            gasUsed = 0
            gasTextField.text?.removeAll()
            gasSetButton.enabled = false
            break
        case timeAddButton:
            travelTime = travelTime + (timeTextField.text! as NSString).floatValue
            timeTextField.text = "\(travelTime)"
            timeTextField.textColor = UIColor.lightGrayColor()
            timeAddButton.hidden = true
            timeSetButton.enabled = true
            break
        case timeSetButton:
            travelTime = 0
            timeTextField.text?.removeAll()
            timeSetButton.enabled = false
            break
        case priceAddButton:
            pricePaidForGas = pricePaidForGas + (priceTextField.text! as NSString).floatValue
            priceTextField.text = "\(pricePaidForGas)"
            priceTextField.textColor = UIColor.lightGrayColor()
            priceAddButton.hidden = true
            priceSetButton.enabled = true
            break
        case priceSetButton:
            pricePaidForGas = 0
            priceTextField.text?.removeAll()
            priceSetButton.enabled = false
            break
        default:
            break
        }
        calculateStatistics()
        view.endEditing(true)
    }
    
    /*  calculateStatistics
    =============================
        This method is called by other functions to
        force all available calculations to be made,
        based on which variables have values.
        It performs the following tasks:
            +updating labels for calculations
    */
    func calculateStatistics() {
        //calculate average speed
        if travelTime > 0 {
            averageSpeedLabel.text = decimalFormatter.stringFromNumber(distanceTravelled / travelTime)! + " mph"
        }
        else {
            averageSpeedLabel.text = "0 mph"
        }
        //calculate remaining distance
        if initialDistance - distanceTravelled > 0 {
            remainingDistanceLabel.text = decimalFormatter.stringFromNumber(initialDistance - distanceTravelled)! + " miles"
            progressBar.progress = distanceTravelled / initialDistance
        }
        else {
            remainingDistanceLabel.text = "0 miles"
            progressBar.progress = distanceTravelled == 0 ? 0 : 1
        }
        
        //calculate remaining time
        if (distanceTravelled > 0 && initialDistance - distanceTravelled >= 0) {
            remainingTimeLeftLabel.text = decimalFormatter.stringFromNumber(travelTime / distanceTravelled * (initialDistance - distanceTravelled))! + " hours"
        }
        else if distanceTravelled >= 0 {
            remainingTimeLeftLabel.text = "0 hours"
        }
        
        //calculate miles per gallon
        if (gasUsed > 0) {
            milesPerGallonLabel.text = decimalFormatter.stringFromNumber(distanceTravelled / gasUsed)! + " mpg"
        }
        else {
            milesPerGallonLabel.text = "0 mpg"
        }
        //calculate gas money per mile
        if (distanceTravelled > 0) {
            gasMoneyPerMileLabel.text = currencyFormatter.stringFromNumber(pricePaidForGas / distanceTravelled)
        }
        else {
            gasMoneyPerMileLabel.text = "$0.00"
        }
        //calculate remaining gas cost
        if (distanceTravelled > 0 && initialDistance - distanceTravelled >= 0) {
            remainingGasCostLabel.text = currencyFormatter.stringFromNumber(pricePaidForGas / distanceTravelled * (initialDistance - distanceTravelled))
        }
        else if distanceTravelled >= 0 {
            remainingGasCostLabel.text = "$0.00"
        }
        replaceEmptyFields() //needed if the add button of another field is pressed and the current is blank
    }
    

    /*  textFieldShouldReturn
    =============================
        This method is called when the return button
        of a keyboard is pressed.
        It performs the following tasks:
            +setting member variables
            +updating text fields text and color
            +switching the active text field
    */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        //move to next field, unless it's the last one
        switch textField {
        case distanceTextField:
            if distanceTextField.text == "" && initialDistance > 0 {
                distanceTextField.textColor = UIColor.lightGrayColor()
                distanceTextField.text = "\(initialDistance)"
            }
            travelledTextField.becomeFirstResponder()
            break
        case travelledTextField:
            if travelledTextField.text == "" && distanceTravelled > 0 {
                travelledTextField.textColor = UIColor.lightGrayColor()
                travelledTextField.text = "\(distanceTravelled)"
            }
            gasTextField.becomeFirstResponder()
            break
        case gasTextField:
            if gasTextField.text == "" && gasUsed > 0 {
                gasTextField.textColor = UIColor.lightGrayColor()
                gasTextField.text = "\(gasUsed)"
            }
            timeTextField.becomeFirstResponder()
            break
        case timeTextField:
            if timeTextField.text == "" && travelTime > 0 {
                timeTextField.textColor = UIColor.lightGrayColor()
                timeTextField.text = "\(travelTime)"
            }
            priceTextField.becomeFirstResponder()
            break
        case priceTextField:
            if priceTextField.text == "" && pricePaidForGas > 0 {
                priceTextField.textColor = UIColor.lightGrayColor()
                priceTextField.text = "\(pricePaidForGas)"
            }
            view.endEditing(true)
            break
        default:
            break
        }
        return true;
    }
    
    /*  textFieldShouldBeginEditing
    =============================
        This method is called before any text field
        gains focus.
        It performs the following tasks:
            +restoring text to any text field that is blank
    */
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        replaceEmptyFields()
        return true
    }
    
    /*  textFieldDidEndEditing
    =============================
        This method is called when any text field
        loses focus.
        It performs the following tasks:
            +simulating an "add" button press when applicable
    */
    func textFieldDidEndEditing(textField: UITextField) {
        switch textField {
        case distanceTextField:
            if initialDistance != (textField.text! as NSString).floatValue {
                UpdateStatistics(distanceAddButton)
            }
            break
        case travelledTextField:
            if distanceTravelled != (textField.text! as NSString).floatValue {
                UpdateStatistics(travelledAddButton)
            }
            break
        case gasTextField:
            if gasUsed != (textField.text! as NSString).floatValue {
                UpdateStatistics(gasAddButton)
            }
            break
        case timeTextField:
            if travelTime != (textField.text! as NSString).floatValue {
                UpdateStatistics(timeAddButton)
            }
            break
        case priceTextField:
            if pricePaidForGas != (textField.text! as NSString).floatValue {
                UpdateStatistics(priceAddButton)
            }
        default:
            break
        }
    }
    
    /*  textFieldDidBeginEditing
    =============================
        This method is called when any text field
        gains focus and is editable.
        It performs the following tasks:
            +clearing out text that is already stored in a variable
                when beginning to edit the field
    */
    func textFieldDidBeginEditing(textField: UITextField) {
        switch textField {
        case distanceTextField:
            if initialDistance != (textField.text! as NSString).floatValue {
                return
            }
        case travelledTextField:
            if distanceTravelled != (textField.text! as NSString).floatValue {
                return
            }
        case gasTextField:
            if gasUsed != (textField.text! as NSString).floatValue {
                return
            }
        case timeTextField:
            if travelTime != (textField.text! as NSString).floatValue {
                return
            }
        case priceTextField:
            if pricePaidForGas != (textField.text! as NSString).floatValue {
                return
            }
        default:
            break
        }
        textField.textColor = UIColor.blackColor()
        textField.text?.removeAll()
    }
    
    /*  textField, shouldChangeCharactersInRange
    =============================
        This method is called when any text field receives input
        in order to validate input.
        It performs the following tasks:
            +Validating the input as a positive number
                (in this case it is < 10000 with a max of 2 decimal places)
    */
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        l bbbbbbbbbbbet oldString:NSString = textField.text ?? "" as NSString
        let candidate:String = oldString.stringByReplacingCharactersInRange(range, withString: string)
        let regex = try? NSRegularExpression(pattern: "^\\d{0,4}(\\.\\d{0,2}?)?$", options: [])
        //returns true if the regular expression is valid
        if regex?.firstMatchInString(candidate, options: [], range: NSRange(location: 0, length: candidate.characters.count)) != nil {
            //determine whether the corresponding button should be active
            switch textField {
            case distanceTextField:
                distanceAddButton.hidden = (candidate.isEmpty || (candidate as NSString).floatValue + initialDistance >= 10000) ? true : false
                break
            case travelledTextField:
                travelledAddButton.hidden = (candidate.isEmpty || (candidate as NSString).floatValue + distanceTravelled > initialDistance) ? true : false
                break
            case gasTextField:
                gasAddButton.hidden = (candidate.isEmpty || (candidate as NSString).floatValue + distanceTravelled >= 10000) ? true : false
                break
            case timeTextField:
                timeAddButton.hidden = (candidate.isEmpty || (candidate as NSString).floatValue + distanceTravelled >= 10000) ? true : false
                break
            case priceTextField:
                priceAddButton.hidden = (candidate.isEmpty || (candidate as NSString).floatValue + distanceTravelled >= 10000) ? true : false
                break
            default:
                break
            }
            return true
        }
        return false
    }
    
    /*  adjustViewForKeyboard
    =============================
        This method is called when a notification that the keyboard
        is going to appear or disappear.
        It performs the following tasks:
            +Adjusting the view to account for the keyboard on screen
    */
    func adjustViewForKeyboard(notification: NSNotification) {
        //grab the notification dictionary
        let userInfo:NSDictionary = notification.userInfo!
        //get the size of the keyboard being loaded
        let keyboardScreenEndFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        //account for view rotations
        let keyboardViewEndFrame:CGRect = view.convertRect(keyboardScreenEndFrame, fromView: view.window)
        //determine if the keyboard is appearing or disappearing and adjust the view
        if notification.name == UIKeyboardWillHideNotification {
            //reset scrolling area
            scrollView.contentInset = UIEdgeInsetsZero
        }
        else {
            //calculate scrolling area
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        //update scroll bar range
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    func dismissKeyboard() {
        replaceEmptyFields()
        //resigns first responder status for the UIView (aka hides the keyboard)
        view.endEditing(true)
    }
    
    //fill in any empty text fields if possible
    /*  replaceEmptyFields
    =============================
        This method is called to replace any empty text fields with
        its corresponding variable's current value.
        It performs the following tasks:
            +filling in blank text fields when applicable
    */
    func replaceEmptyFields() {
        if distanceTextField.text == "" && initialDistance > 0 {
            distanceTextField.textColor = UIColor.lightGrayColor()
            distanceTextField.text = "\(initialDistance)"
        }
        else if travelledTextField.text == "" && distanceTravelled > 0 {
            travelledTextField.textColor = UIColor.lightGrayColor()
            travelledTextField.text = "\(distanceTravelled)"
        }
        else if gasTextField.text == "" && gasUsed > 0 {
            gasTextField.textColor = UIColor.lightGrayColor()
            gasTextField.text = "\(gasUsed)"
        }
        else if timeTextField.text == "" && travelTime > 0 {
            timeTextField.textColor = UIColor.lightGrayColor()
            timeTextField.text = "\(travelTime)"
        }
        else if priceTextField.text == "" && pricePaidForGas > 0 {
            priceTextField.textColor = UIColor.lightGrayColor()
            priceTextField.text = "\(pricePaidForGas)"
        }
    }
}


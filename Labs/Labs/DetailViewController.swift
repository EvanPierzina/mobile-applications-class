//
//  DetailViewController.swift
//  Labs
//
//  Created by Evan Pierzina on 2/19/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation

class DetailViewController: UIViewController, UITabBarDelegate {
    
    @IBOutlet weak var labName: UINavigationItem!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var labDescription: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var ttsButton: UIButton!
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var infoTab: UITabBarItem!
    @IBOutlet weak var mapTab: UITabBarItem!
    
    var lab: Lab?
    var utterence = AVSpeechUtterance()
    let synthesizer = AVSpeechSynthesizer()
    
    var detailItem: AnyObject? {
        didSet {
            // Update the view whenever this variable is changed.
            self.configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        //        if let detail = self.detailItem {
        //            if let label = self.detailDescriptionLabel {
        //                label.text = detail.description
        //            }
        //        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
        
        if lab != nil {
            //general setup
            labDescription.text = lab?.getDescription()
            labDescription.font = UIFont(name: "Helvetica Neue", size: 18)
            imageView.image = UIImage(named: lab!.getName())
            utterence = AVSpeechUtterance(string: lab!.getDescription())
            
            //tab bar stuff
            tabBar.tintColor = UIColor(red: 174.0/255, green: 31.0/255, blue: 34.0/255, alpha: 1.0)
            tabBar.selectedItem = infoTab
            
            //map stuff
            mapView.setRegion(MKCoordinateRegionMakeWithDistance(lab!.getCoordinates().coordinate, 500, 500), animated: true)
            mapView.mapType = .HybridFlyover
            let pin = MKPointAnnotation()
            pin.coordinate = CLLocationCoordinate2D(latitude: lab!.getCoordinates().coordinate.latitude, longitude: lab!.getCoordinates().coordinate.longitude)
            pin.title = lab?.getName()
            pin.subtitle = "University of Nevada, Las Vegas"
            mapView.addAnnotation(pin)
            mapView.selectAnnotation(pin, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        if synthesizer.speaking {
            synthesizer.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
        }
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        if item == infoTab {
            mapView.hidden = true
            imageView.hidden = false
            labDescription.hidden = false
            ttsButton.hidden = false
        }
        else {
            mapView.hidden = false
            imageView.hidden = true
            labDescription.hidden = true
            ttsButton.hidden = true
        }
    }
    
    @IBAction func textToSpeechButtonPressed(sender: UIButton) {
        if synthesizer.speaking {
            synthesizer.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
        }
        else {
            synthesizer.speakUtterance(utterence)
        }
    }
    
}


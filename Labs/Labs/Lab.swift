//
//  Lab.swift
//  Labs
//
//  Created by Evan Pierzina on 2/19/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation

class Lab
{
    private var name: String?
    private var description: String?
    private var detailImage: UIImage?
    private var coordinates: CLLocation?
    
    init(name: String, description: String, latitude: Double, longitude: Double)
    {
        self.name = name
        self.description = description
        self.detailImage = UIImage(named: name)
        self.coordinates = CLLocation(latitude: latitude, longitude: longitude)
    }
    
    func getName() -> String {
        return name!
    }
    
    func getDescription() -> String {
        return description!
    }
    
    func getImage() -> UIImage {
        return detailImage!
    }
    
    func getCoordinates() -> CLLocation {
        return coordinates!
    }
}
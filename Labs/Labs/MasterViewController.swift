//
//  MasterViewController.swift
//  Labs
//
//  Created by Evan Pierzina on 2/19/16.
//  Copyright © 2016 Evan Pierzina. All rights reserved.
//

import UIKit
import MapKit

class MasterViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var viewTitle: UINavigationItem!
    var detailViewController: DetailViewController? = nil
    
    var labs = [Lab]()
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var table: UITableView!
    var searchIsActive : Bool = false
    var filteredResults: [Lab] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        //read from plist to generate Lab objects
        if let dataPath = NSBundle.mainBundle().pathForResource("Labs", ofType: "plist") {
            if let dataArray = NSArray(contentsOfFile: dataPath) {
                let labArray = dataArray as [AnyObject] as!  [Dictionary<String, String>]
                
                //generate the labs array
                for lab in labArray {
                    labs.append(Lab(name: lab["name"]!, description: lab["description"]!, latitude: (lab["latitude"]! as NSString).doubleValue, longitude: (lab["longitude"]! as NSString).doubleValue))
                }
            }
        }
    }

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let lab = searchIsActive ? filteredResults[indexPath.row] : labs[indexPath.row]
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                controller.labName.title = lab.getName()
                controller.lab = lab
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searchIsActive) {
            return filteredResults.count
        }
        else {
            return labs.count
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        if (searchIsActive) {
            cell.textLabel?.text = filteredResults[indexPath.row].getName()
        }
        else {
            cell.textLabel!.text = labs[indexPath.row].getName()
        }
        return cell
    }

    // MARK: - Search Bar
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        if searchBar.text != "" {
            searchIsActive = true
        }
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchIsActive = false
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchIsActive = false
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchIsActive = false
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            searchIsActive = false
        }
        else {
            filteredResults = labs.filter {
                $0.getName().rangeOfString(searchText, options: .CaseInsensitiveSearch) != nil
            }
            searchIsActive = true
        }
        self.tableView.reloadData()
    }
}

